import Vue from "vue";
import VueRouter from "vue-router";
import QuestionsView from "../components/QuestionsView.vue"
import StartPage from "../components/StartPage.vue"
import ResultsView from "../components/ResultsView.vue"

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Start",
    component: StartPage,
  },
  {
    path: "/play",
    name: "Play",
    component: QuestionsView
  },
  {
    path: "/results",
    name: "Results",
    component: ResultsView
  }

];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
