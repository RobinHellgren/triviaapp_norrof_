//Iterates over all values in the JSON and calls the decode HTML entity function for each string.
export default function decodeJSONStrings(json){
    let decodedJSON = []
    json.forEach(row => {
        let decodedRow = {
            category: decodeHTMLEntity(row.category),
            type: decodeHTMLEntity(row.type),
            difficulty: decodeHTMLEntity(row.difficulty),
            question: decodeHTMLEntity(row.question),
            correct_answer: decodeHTMLEntity(row.correct_answer),
            incorrect_answers: []
        };
        row.incorrect_answers.forEach(element => {
            decodedRow.incorrect_answers.push(decodeHTMLEntity(element))
        });
        decodedJSON.push(decodedRow);
    });
    return decodedJSON
}
//Decode HTML entity strings by wrapping them in a textarea and returns their contained value.
function decodeHTMLEntity(entity){
    const textarea = document.createElement('textarea');
    textarea.innerHTML = entity;
    return textarea.value;
}