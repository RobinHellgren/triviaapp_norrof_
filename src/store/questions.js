import Vue from "vue";
import Vuex from "vuex";
import decodeJSONStrings from '../utils/decode.js'
const BASE_API_URL = "https://opentdb.com/api.php?";
const CATEGORIES_API_URL = "https://opentdb.com/api_category.php";

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        apiParams: [],
        questionList: [],
        currentQuestion: 0,
        score: 0,
        selectedCategory: 0,
        questionAnswer: [],
        categoryList: [],
    },
    getters: {
        getApiParams: state => {
            return state.apiParams;
        },
        getQuestionList: state => {
            return state.questionList;
        },
        getScore: state => {
            return state.score;
        },
        getSelectedCategory: state => {
            return state.selectedCategory;
        },
        //Returns type of the current question - if it is not loaded - returns "Loading" instead.
        getQuestionType: state => {
            if (state.questionList[state.currentQuestion] == undefined) {
                return "loading";
            }
            return state.questionList[state.currentQuestion].type;
        },
    },
    mutations: {
        pushParams(state, value) {
            state.apiParams = {};
            state.apiParams = value;
        },
        saveQuestions(state, value) {
            value = decodeJSONStrings(value)
            console.log(value)
            state.questionList = value;
        },
        setCurrentQuestion(state, value) {
            state.currentQuestion = value;
        },
        increaseScore(state, increase) {
            state.score += increase;
        },
        addAnswer(state, questionAnswer) {
            state.questionAnswer.push(questionAnswer);
        },
        resetApiParams(state) {
            state.apiParams = []
        },
        resetQuestionList(state) {
            state.questionList = []
        },
        resetCurrentQuestion(state) {
            state.currentQuestion = 0
        },
        resetScore(state) {
            state.score = 0
        },
        resetQuestionAnswer(state) {
            state.questionAnswer = []
        },
        setCategoryList(state, value) {
            state.categoryList = value;
        }
    },
    actions: {
        // Builds the URL for the API call based on the parameters of the previous input.
        async getQuestionsFromApi(context) {
            let url = BASE_API_URL + "amount=" +
                context.state.apiParams.numberOfQuestions +
                "&category=" + context.state.apiParams.selectedCategory +
                "&difficulty=" + context.state.apiParams.selectedDifficulty;

            let response;
            try {
                response = await fetch(url)
                    .then(response => response.json())
            } catch(error) {
                console.log("Failed to get questions from the API - " + error);
            }
            context.commit("saveQuestions", response.results);

        },
        //Builds the URL for getting the categories from the API.
        async getCategoriesFromApi(context) {
            let url = CATEGORIES_API_URL;

            let response;
            try {
                response = await fetch(url)
                    .then(response => response.json())
            } catch (error) {
                console.log("Failed to get categories from the API - " + error);
            }
            context.commit("setCategoryList", response.trivia_categories);
        }
    },
    modules: {},
});