export function getCategories() {
    return {
        "trivia_categories": [{
                "id": 9,
                "name": "General Knowledge"
            },

            {
                "id": 10,
                "name": "Entertainment: Books"
            },

            {
                "id": 11,
                "name": "Entertainment: Film"
            },

            {
                "id": 12,
                "name": "Entertainment: Music"
            },

            {
                "id": 13,
                "name": "Entertainment: Musicals & Theatres"
            },

            {
                "id": 14,
                "name": "Entertainment: Television"
            },

            {
                "id": 15,
                "name": "Entertainment: Video Games"
            },

            {
                "id": 16,
                "name": "Entertainment: Board Games"
            },

            {
                "id": 17,
                "name": "Science & Nature"
            },

            {
                "id": 18,
                "name": "Science: Computers"
            },

            {
                "id": 19,
                "name": "Science: Mathematics"
            },

            {
                "id": 20,
                "name": "Mythology"
            },

            {
                "id": 21,
                "name": "Sports"
            },

            {
                "id": 22,
                "name": "Geography"
            },

            {
                "id": 23,
                "name": "History"
            },

            {
                "id": 24,
                "name": "Politics"
            },

            {
                "id": 25,
                "name": "Art"
            },

            {
                "id": 26,
                "name": "Celebrities"
            },

            {
                "id": 27,
                "name": "Animals"
            },

            {
                "id": 28,
                "name": "Vehicles"
            },

            {
                "id": 29,
                "name": "Entertainment: Comics"
            },

            {
                "id": 30,
                "name": "Science: Gadgets"
            },

            {
                "id": 31,
                "name": "Entertainment: Japanese Anime & Manga"
            },

            {
                "id": 32,
                "name": "Entertainment: Cartoon & Animations"
            }
        ]
    }
}
export function getQuestions() {

    return [{
            "category": "Science: Mathematics",
            "type": "multiple",
            "difficulty": "medium",
            "question": "In the hexadecimal system,what number comes after 9?",
            "correct_answer": "The Letter A",
            "incorrect_answers": ["10", "The Number 0", "16"]
        },

        {
            "category": "Science: Mathematics",
            "type": "multiple",
            "difficulty": "medium",
            "question": "What is the area of a circle with a diameter of 20 inches if &pi;= 3.1415?",
            "correct_answer": "314.15 Inches",
            "incorrect_answers": ["380.1215 Inches", "3141.5 Inches", "1256.6 Inches"]
        },

        {
            "category": "Science: Mathematics",
            "type": "multiple",
            "difficulty": "medium",
            "question": "What are the first 6 digits of the number &quot;Pi&quot;?",
            "correct_answer": "3.14159",
            "incorrect_answers": ["3.14169", "3.12423", "3.25812"]
        },

        {
            "category": "Science: Mathematics",
            "type": "multiple",
            "difficulty": "medium",
            "question": "What is the Roman numeral for 500?",
            "correct_answer": "D",
            "incorrect_answers": ["L", "C", "X"]
        },

        {
            "category": "Science: Mathematics",
            "type": "boolean",
            "difficulty": "medium",
            "question": "111,111,111 x 111,111,111 = 12,345,678,987,654,321",
            "correct_answer": "True",
            "incorrect_answers": ["False"]
        },

        {
            "category": "Science: Mathematics",
            "type": "multiple",
            "difficulty": "medium",
            "question": "What is the alphanumeric representation of the imaginary number?",
            "correct_answer": "i",
            "incorrect_answers": ["e", "n", "x"]
        },

        {
            "category": "Science: Mathematics",
            "type": "multiple",
            "difficulty": "medium",
            "question": "To the nearest whole number,how many radians are in a whole circle?",
            "correct_answer": "6",
            "incorrect_answers": ["3", "4", "5"]
        },

        {
            "category": "Science: Mathematics",
            "type": "multiple",
            "difficulty": "medium",
            "question": "What Greek letter is used to signify summation?",
            "correct_answer": "Sigma",
            "incorrect_answers": ["Delta", "Alpha", "Omega"]
        },

        {
            "category": "Science: Mathematics",
            "type": "multiple",
            "difficulty": "medium",
            "question": "Which greek mathematician ran through the streets of Syracuse naked while shouting &quot;Eureka&quot; after discovering the principle of displacement?",
            "correct_answer": "Archimedes",
            "incorrect_answers": ["Euclid", "Homer", "Eratosthenes"]
        },

        {
            "category": "Science: Mathematics",
            "type": "multiple",
            "difficulty": "medium",
            "question": "How many books are in Euclid&#039;s Elements of Geometry?",
            "correct_answer": "13",
            "incorrect_answers": ["8", "10", "17"]
        }
    ]
}